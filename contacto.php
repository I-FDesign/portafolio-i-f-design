<?php 
	function limpiarDatos($variable){
		$variable = trim($variable);
		$variable = filter_var($variable, FILTER_SANITIZE_STRING);
		return $variable;
	}

	$errores = '';
	$enviado = '';

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if($_POST['nombre'] !== '' && $_POST['correo'] !== '' && $_POST['mensaje'] !== ''){
			$nombre = $_POST['nombre'];
			$correo = $_POST['correo'];
			$mensaje = $_POST['mensaje'];
			if($_POST['asunto']){
				$asunto = $_POST['asunto'];
			}else{
				$asunto = 'Enviado desde mi portafolios';
			}
		}
		else{
			$errores.= 'Completa los campos requeridos';
		}

		$nombre = limpiarDatos($nombre);
		$asunto = limpiarDatos($asunto);
		$mensaje = limpiarDatos($mensaje);		

		if(filter_var($correo, FILTER_VALIDATE_EMAIL)){
			$correo= filter_var($correo, FILTER_SANITIZE_EMAIL);
		}else{
			$errores.= 'El mail ingresado no es valido';
		}

		if(empty($errores)){
			$enviar_a = 'santupa767@gmail.com';
			$mensaje_preparado = "De: $nombre \n";
			$mensaje_preparado .= "Asunto: $asunto \n";
			$mensaje_preparado .= "Mensaje: $mensaje";
			mail($enviar_a, $asunto, $mensaje_preparado);
			$enviado.= true;
		}
	}

 ?>