<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Portafolio</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,600i,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="estilos/normalize.css">
	<link rel="stylesheet" href="estilos/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="estilos/estilos.css">
</head>
<body>
	<header>

		<div class="contenedor-mob">
			<div class="menu" id="menu">
				<div class="icon" id='icon'>
					<p>Menu</p>
					<i class="fa fa-arrow-down" aria-hidden="true"></i>
				</div>
				<nav id='menu-nav'>
					<a href="#">Acerca de</a>
					<a href="#">Trabajos</a>
					<a href="#" class="contact">Contacto</a>
				</nav>
			</div>
			<div class="contenedor_titulo">
				<div class="titulo">
					<h1>I & F Design</h1>
					<h2>Desarrollos web</h2>
				</div>
			</div>
		</div>
	</header>

	<section class="main">
		<div class="contenedor">
			<div class="acercade">
				<div class="foto">
					<img src="img/foto.jpg" width="115" height="115" alt="Yo">
				</div>
				<div class="info">
					<h1>Acerca De</h1>
					<p>Lorem ipsum dolor sit amet, <span class="bold">consectetur adipisicing elit.</span> Qui mollitia rerum, nemo iusto porro neque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem sequi doloribus perferendis unde nam, cumque.</p>
				</div>
			</div>
			
			<div class="trabajos">
				<div class="titulo">
					<h1>Trabajos</h1>
				</div>
				<div class="works">
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/1.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/2.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/3.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/4.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/5.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/6.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/7.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
					<div class="trabajo">
						<div class="content">
							<img src="img/trabajos/8.jpg" alt="">
							<h3>Lorem ipsum</h3>
							<p>HTML - CSS -PHP</p>
						</div>
					</div>
				</div>
			</div>
			<div class="contacto">
				<div class="titulo">
					<h2>Contacto</h2>
				</div>
			
				<div class="input">
					<form action="contacto.php" method="post">
						<input type="text" class="nombre" placeholder="* Nombre" name='nombre'>
						<input type="text" class="correo" placeholder="* Correo" name='correo'>
						<input type="text" name="asunto" placeholder="Asunto" class= 'asunto'>
						<textarea class="texto" placeholder="* Mensaje" name='mensaje'></textarea>
						<input type="submit" value="Enviar">
					</form>
				</div>
			</div>
		</div>
	</section>

	<footer>
		<div class="contenedor">
			<section class="redes-sociales">
				<a class="twitter" href="http://www.twitter.com/falconmasters"><i class="fa fa-twitter"></i></a>
				<a class="facebook" href="http://www.facebook.com/falconmasters"><i class="fa fa-facebook"></i></a>
				<a class="youtube" href="http://www.youtube.com/falconmasters"><i class="fa fa-youtube-play"></i></a>
				<a class="github" href="http://www.github.com/falconmasters"><i class="fa fa-github"></i></a>
				<a class="instagram" href="http://www.instagram.com/falconmasters"><i class="fa fa-instagram"></i></a>
			</section>
			<section class="copy">
				<p>Desarrolado por I & F Design &copy;</p>
			</section>
		</div>
	</footer>
	<script src='script.js'></script>
</body>
</html>